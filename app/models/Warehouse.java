package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanh on 3/25/2015.
 */

@Entity
public class Warehouse extends Model {
        @Id
        public Long id;
        @OneToOne
        public Address address;
        public String name;

        @OneToMany(mappedBy = "warehouse")
        public static Finder<Long, Warehouse> find = new Finder<Long, Warehouse>(Long.class, Warehouse.class);
        public List<StockItem> stock = new ArrayList();
    public String toString() {
        return name;
    }
}
